<?php

require_once('./include/DB.php');

$uri = 'http://localhost/Ejercicios_PHP_Sergio/Tema_6_1/';


// Creamos el servicio, pasándole los parámetros de creación como un array
$server = new SoapServer(null, array('uri' => $uri));

// Asignamos las funciones que tendrá el servicio
$server->setClass('DB');

// Hacemos que el servicio se encargue de procesar las peticiones
// usando la función handle()
$server->handle();
