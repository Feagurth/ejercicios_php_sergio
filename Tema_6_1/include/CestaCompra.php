<?php

require_once('include/Producto.php');

class CestaCompra {

    protected $productos = array();

    // Introduce un nuevo artículo en la cesta de la compra
    public function nuevo_articulo($codigo) {

        $url = 'http://localhost/Ejercicios_PHP_Sergio/Tema_6_1/servicio.php';
        $uri = 'http://localhost/Ejercicios_PHP_Sergio/Tema_6_1/';

        // Creamos un cliente pasandole las opciones como un array
        $cliente = new SoapClient(null, array('location' => $url, 'uri' => $uri));

        // Recuperamos los productos
        $producto = $cliente->obtieneProducto($codigo);

        // Forzamos la serialización del objeto stdClass a un array y lo usamos 
        // como parámetro para crear el objeto Producto
        $this->productos[] = new Producto(get_object_vars($producto));
    }

    // Obtiene los artículos en la cesta
    public function get_productos() {
        return $this->productos;
    }

    // Obtiene el coste total de los artículos en la cesta
    public function get_coste() {
        $coste = 0;
        foreach ($this->productos as $p) {
            $coste += $p->getPVP();
        }
        return $coste;
    }

    // Devuelve true si la cesta está vacía
    public function vacia() {
        if (count($this->productos) == 0) {
            return true;
        }
        return false;
    }

    // Guarda la cesta de la compra en la sesión del usuario
    public function guarda_cesta() {
        $_SESSION['cesta'] = $this;
    }

    // Recupera la cesta de la compra almacenada en la sesión del usuario
    public static function carga_cesta() {
        if (!isset($_SESSION['cesta'])) {
            return new CestaCompra();
        } else {
            return ($_SESSION['cesta']);
        }
    }

    // Muestra el HTML de la cesta de la compra, con todos los productos
    public function muestra() {
        // Si la cesta está vacía, mostramos un mensaje
        if (count($this->productos) == 0) {
            print "<p>Cesta vacía</p>";
        }
        //  y si no está vacía, mostramos su contenido
        else {
            foreach ($this->productos as $producto) {
                $producto->muestra();
            }
        }
    }

}
