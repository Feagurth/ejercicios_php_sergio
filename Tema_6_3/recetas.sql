create database recetas DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;

use recetas;

CREATE TABLE recetas (
cod_rec INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nombre varchar(50) NOT NULL,
tipo ENUM('Entrante','Primer plato','Segundo plato','Postre','Desayuno','Merienda'),
preparacion TEXT,
presentacion TEXT 
) ENGINE = INNODB;

CREATE TABLE ingredientes (
cod_ing INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
nombre varchar(30) NOT NULL,
cantidad decimal(5,2),
unidad varchar(15),
cod_rec INT,
FOREIGN KEY (cod_rec) REFERENCES recetas(cod_rec)
) ENGINE = INNODB;

CREATE USER 'dwes2' IDENTIFIED BY 'abc123.';
 
GRANT ALL ON recetas.* TO 'dwes2';

insert into recetas (nombre, tipo, preparacion, presentacion) values ('Trufas de chocolate y cereza','Postre','Comienza derritiendo el chocolate al baño María con la mantequilla, removiendo con la ayuda de una espátula de pastelería,
preferiblemente con un movimiento envolvente. Acuérdate de agregarle un chorrito de licor de cereza (u otro tipo de licor que tengamos
por casa, un buen ron o whisky le irá estupendo) al final, cuando la mezcla esté completamente derretida y el chocolate brillante y con
una textura cremosa. Para montar las trufas de cereza y chocolate, pincha las cerezas con un palillo, y báñalas por completo en el chocolate. Ve
colocándolas en una fuente amplia, retira el palillo con cuidado, y déjalas enfriar, al menos dos horas, a temperatura ambiente. Cuando el chocolate haya solidificado por completo, pasa las trufas por cacao en polvo, y el resultado será espectacular',
'Puedes servir cada trufa de chocolate y cereza en un papelito para bombones, creando un bocado dulce casero y apetitoso realizado con
tus propias manos. ¿Qué te parece? ¡Genial!');
insert into ingredientes (nombre, cantidad,unidad,cod_rec) values 
('Cerezas en almíbar',1,'Bote',1),
('Chocolate negro',250,'gramos',1),
('Licor de cereza',1,'Chorrito',1),
('Cacao en polvo',1,'para decorar',1);