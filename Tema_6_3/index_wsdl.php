<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <title>Tema 6: Ejercicio 2</title>
    </head>

    <body>

        <?php
        require_once './include/Ingredientes.php';

        $cliente = new SoapClient('http://localhost/Ejercicios_PHP_Sergio/Tema_6_3/servicio_wsdl.php?wsdl');

        echo $cliente->obtenerNombreReceta(1);
        echo '<br>';
        echo $cliente->obtenerTipoReceta(1);
        echo '<br>';
        echo $cliente->obtenerPreparacionReceta(1);
        echo '<br>';
        echo $cliente->obtenerPresentacionReceta(1);
        echo '<br>';

        $ingredientes = $cliente->obtenerIngredientesReceta(1);

        for ($index = 0; $index < count($ingredientes); $index++) {

            $temporal = get_object_vars($ingredientes[$index]);

            $p = new Ingredientes($temporal);

            echo $p->muestra();
        }
        ?>
    </body>