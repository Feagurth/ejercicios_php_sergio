<!DOCTYPE html>
<?php
include_once './include/Juego.php';

$textoPregunta = "";
$textoRespuesta1 = "";
$textoRespuesta2 = "";
$textoRespuesta3 = "";
$textoRespuesta4 = "";

$juego = new Juego();

$juego->carga_juego();

// Comprobamos si se ha pulsado el botón de nueva pregunta
if (isset($_POST['Aceptar'])) {
    if (isset($_POST['respuesta'])) {
        if ($juego->verificarRespuesta($_POST['respuesta'])) {
            $juego->acierto();
        } else {
            $juego->fallo();
        }
    }
}

// Comprobamos cuantas preguntas tenemos realizadas y si su número es 
// menor que la cantidad de preguntas que tenemos
if ($juego->numeroPreguntasRealizadas() < $juego->numeroPreguntasTotales()) {

    // Volvamos la información del objeto Pregunta a las variables necesarias 
    // para mostrar la información
    $textoPregunta = $juego->obtienePregunta();
    $textoRespuesta1 = $juego->obtieneRespuesta1();
    $textoRespuesta2 = $juego->obtieneRespuesta2();
    $textoRespuesta3 = $juego->obtieneRespuesta3();
    $textoRespuesta4 = $juego->obtieneRespuesta4();
} else {

    // En caso de que no queden preguntas, se ha terminado el juego y se muestra un mensaje con la puntuación
    $textoPregunta = "No quedan preguntas.";
    $textoRespuesta1 = "";
    $textoRespuesta2 = "";
    $textoRespuesta3 = "";
    $textoRespuesta4 = "";
}

$juego->guarda_juego();
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <div>
                <label>Datos del juego --- Total Preguntas: <?php echo $juego->numeroPreguntasRealizadas() + 1 ?> -- Total Aciertos: <?php echo $juego->numeroAciertos() ?></label>
            </div>

            <div>
                <label><?php echo $textoPregunta ?></label>
            </div>
            <?php
            if ($textoPregunta !== "No quedan preguntas.") {

                echo '<form id="formPreguntas" action="index.php" method="POST">';

                echo '<div>';

                if (!empty($textoRespuesta1)) {
                    echo '<input type="radio" name="respuesta" value="1">';
                    echo $textoRespuesta1;
                    echo '</input>';
                }
                if (!empty($textoRespuesta2)) {
                    echo '<input type="radio" name="respuesta" value="2">';
                    echo $textoRespuesta2;
                    echo '</input>';
                }
                if (!empty($textoRespuesta3)) {
                    echo '<input type="radio" name="respuesta" value="3">';
                    echo $textoRespuesta3;
                    echo '</input>';
                }
                if (!empty($textoRespuesta4)) {
                    echo '<input type="radio" name="respuesta" value="4">';
                    echo $textoRespuesta4;
                    echo '</input>';
                }

                echo '</div>';
                echo '<div>';
                echo '<input form="formPreguntas" type="submit" name="Aceptar" id="Aceptar" value="Aceptar" />';
                echo '</div>';
                echo '</form>';
            } else {

                $juego->reiniciarJuego();

                echo '<form id="formNuevo" action="index.php" method="POST">';
                echo '<input form="formNuevo" type="submit" name="Nuevo" id="Nuevo" value="Nuevo Juego" />';
                echo '</form>';
            }
            ?>
        </div>
    </body>
</html>
