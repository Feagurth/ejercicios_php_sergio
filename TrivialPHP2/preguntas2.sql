-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-03-2016 a las 23:13:01
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trivial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `preguntas2`
--
drop database if exists trivial;
create database trivial;
use trivial;
CREATE TABLE `preguntas` (
  `numero` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `pregunta` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `respuesta1` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `respuesta2` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `respuesta3` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `respuesta4` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `respuesta` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `preguntas2`
--

INSERT INTO `preguntas` (`numero`, `pregunta`, `respuesta1`, `respuesta2`, `respuesta3`, `respuesta4`, `respuesta`) VALUES
(1, '¿Cuántas unidades tiene este módulo?', '6', '8', '10', '4', 2),
(2, 'En el foro de cada unidad, con carácter general:', 'Nunca se valorarán las aportaciones del alumnado.', 'Sólo se valoraran las respuestas del profesor', 'Se valorará las aportaciones interesantes', 'Todas las respuestas anteriores son falsas', 3),
(3, 'La nota final de pruebas presenciales será:', '40% el primer cuatrimestre y 60% el segundo', 'La media aritmética de los dos cuatrimestres', '60% el primer cuatrimestre y 40% el segundo', 'Sólo puntúa el segundo cuatrimestre', 1),
(4, '¿Cuál es la fecha tope para la tarea  2', 'El 4 de noviembre', 'El 23 de diciembre', 'El 18 de enero', 'El 3 de febrero', 3),
(5, 'Indica la respuesta correcta', 'Podemos enviarle la tarea al correo del profe', 'Si nos retrasamos usamos el correo postal', 'Lo avisamos por teléfono', 'Ninguna es correcta', 4),
(6, 'Para que un ordenador pueda ejecutar un programa', 'Necesitamos un editor de texto', 'Necesitamos a un ingeniero', 'Lo seleccionamos en el escritorio', 'Llamamos a Juan Simón', 4),
(7, '¿Qué es un servicio?', 'Programas que hacen que funcione el hardware', 'Un programa que corre en segundo plano ', 'onjunto formado por el software y el hardware', 'Programa para poder navegar por Internet', 2),
(8, '¿Qué lenguaje utilizan para crear una página web?', 'PHP', 'Cobol', 'Pascal', 'XML', 1),
(9, '¿Qué es Apache?', 'Un indio', 'Un cliente web', 'Una variable global de PHP', 'Ninguna es correcta', 4),
(10, 'Javascript es', 'Un navegador', 'Un protocolo', 'Un servicio', 'Un lenguaje', 4);

--
-- �?ndices para tablas volcadas
--

--
-- Indices de la tabla `preguntas2`
--
/*ALTER TABLE `preguntas`
  ADD PRIMARY KEY (`numero`);*/

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;