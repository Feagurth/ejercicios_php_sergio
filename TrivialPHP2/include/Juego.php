<?php

include_once('DBTrivial.php');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Juego
 *
 * @author Sergio
 */
class Juego {

    private $preguntas;
    private $realizadas;
    private $pregunta_actual;

    public function __construct() {

        If (session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }

        $this->preguntas = array();
        $this->realizadas = array();
    }

    public function getPreguntas() {
        return $this->preguntas;
    }

    public function acierto() {
        $this->preguntas[] = TRUE;
    }

    public function fallo() {
        $this->preguntas[] = FALSE;
    }

    public function guarda_juego() {
        $_SESSION['juego'] = $this->preguntas;
        $_SESSION['realizadas'] = $this->realizadas;
    }

    public function carga_juego() {
        if (isset($_SESSION['juego'])) {
            $this->preguntas = $_SESSION['juego'];
        }

        if (isset($_SESSION['realizadas'])) {
            $this->realizadas = $_SESSION['realizadas'];
        }
    }

    public function numeroPreguntasTotales() {

        $cliente = new DBTrivial();

        $valor = $cliente->listarNumeroPreguntas();

        return $valor;
    }

    public function numeroPreguntasRealizadas() {
        $valor = count($this->preguntas);

        return $valor;
    }

    public function numeroAciertos() {
        $contador = 0;

        for ($index = 0; $index < count($this->preguntas); $index++) {
            if ($this->preguntas[$index] === TRUE) {
                $contador++;
            }
        }

        return $contador;
    }

    public function numeroErrores() {
        $contador = 0;

        for ($index = 0; $index < count($this->preguntas); $index++) {
            if ($this->preguntas[$index] === FALSE) {
                $contador++;
            }
        }

        return $contador;
    }

    public function obtienePregunta() {

        $numMaximo = $this->numeroPreguntasTotales();

        do {
            $numeroPregunta = mt_rand(1, $numMaximo);
        } while (in_array($numeroPregunta, $this->realizadas));

        $cliente = new DBTrivial();

        $salida = $cliente->obtienePregunta($numeroPregunta);

        $this->realizadas[] = $numeroPregunta;

        $this->pregunta_actual = $numeroPregunta;

        return $salida;
    }

    public function obtieneRespuesta1() {

        $cliente = new DBTrivial();

        return $cliente->obtieneRespuesta1($this->pregunta_actual);
    }

    public function obtieneRespuesta2() {

        $cliente = new DBTrivial();

        return $cliente->obtieneRespuesta2($this->pregunta_actual);
    }

    public function obtieneRespuesta3() {

        $cliente = new DBTrivial();

        return $cliente->obtieneRespuesta3($this->pregunta_actual);
    }

    public function obtieneRespuesta4() {

        $cliente = new DBTrivial();

        return $cliente->obtieneRespuesta4($this->pregunta_actual);
    }

    public function verificarRespuesta($respuesta) {
        $cliente = new DBTrivial();

        $pregunta_anterior = $this->realizadas[count($this->realizadas) - 1];

        return $cliente->verificarRespuesta($pregunta_anterior, $respuesta);
    }

    public function reiniciarJuego() {
        unset($_SESSION['juego']);
        unset($_SESSION['realizadas']);
    }

}
